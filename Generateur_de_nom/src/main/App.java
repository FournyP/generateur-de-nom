package main;

import java.util.ArrayList;
import java.util.List;

public class App {

	/**
	 * Fonction vérifiant la validité de la chaine de caractere saisie par l'utilisateur
	 * @param nom_prenom : La chaine de caractere saisie
	 * @return : Boolean, true si la saisie est bonne, false si la saisie est mauvaise
	 */
	public static boolean ValiditeChaine(String nom_prenom){
		boolean res_caract = true;
		int indice_chaine = 0;
		int nb_caractere_unspace = 0;

		//On test si tous les caractères sont bien des lettres grâce à la table ascii
		while(res_caract == true && indice_chaine < nom_prenom.length()){
			if(nom_prenom.charAt(indice_chaine) < 65 && nom_prenom.charAt(indice_chaine) != 32){
				res_caract = false;
			}
			if(nom_prenom.charAt(indice_chaine) < 97 && nom_prenom.charAt(indice_chaine) > 90){
				res_caract = false;
			}
			if(nom_prenom.charAt(indice_chaine) > 122){
				res_caract = false;
			}
			//On compte également le nombre de caractere qui ne sont pas des espace
			if(res_caract == true && nom_prenom.charAt(indice_chaine) != 32){
				nb_caractere_unspace++;
			}
			indice_chaine++;	
		}
		indice_chaine = 0;
		boolean res_space = false;
		int nb_caractere_prenom = 0;
		//On test ensuite s'il y a bien un espace (pour nom puis prenom)
		while(res_caract == true && indice_chaine < nom_prenom.length() && res_space == false){
			if(nom_prenom.charAt(indice_chaine) == 32){
				res_space = true;
			}
			else{
				nb_caractere_prenom++;
			}
			indice_chaine++;
		}

		//On retourne true que si res_caract et res_space le sont tout les deux
		return res_caract && res_space && nb_caractere_unspace>=4 && nb_caractere_prenom>=3;
	}

	/**
	 * Fonction permettant de savoir si le nom est rick ou morty (Utilisable que pour l'univers de Rick et Morty)
	 * @param nom : Le nom de famille saisie par l'utilisateur
	 * @return : Rick ou Morty selon son nom de famille
	 */
	public static String RickorMorty(String nom){
		if(nom.charAt(0)%2 == 0){
			return "Rick";
		}
		else{
			return "Morty";
		}
	}

	/**
	 * Fonction retournant un nom ou prenom fictionnel selon son nom ou prenom actuel ainsi que
	 * selon la fiction 
	 * @param all_name : La liste des nom dans la fiction (De A à Z)
	 * @param name : Le nom actuel que l'utilisateur a saisie
	 * @return : Le nom/prenom fictionnel
	 */
	public static String get_new_name(List<String> all_name, String name){
		String new_name = "";

		int indice_alphabet = 0;
		boolean name_is_set = false;
		char first_letter_prenom = name.charAt(0);

		//On passe first_letter_prenom en majuscule
		if(first_letter_prenom >= 97 && first_letter_prenom <= 122){
			first_letter_prenom =(char)((int)first_letter_prenom - 32);
		}

		//On attribut le nouveau prénom à res
		while(name_is_set == false && indice_alphabet < 26){
			char caract = (char) (indice_alphabet + 65);
			if(first_letter_prenom == caract){
				new_name = all_name.get(indice_alphabet);
				name_is_set = true;
			}
			indice_alphabet++;
		}

		return new_name;
	}

	/**
	 * Fonction retournant un nom fictif selon l'univers de Rick et Morty
	 * Le prénom est l'un d'une liste de 26 (Ce sont des adjectifs)
	 * Le nom de famille est soit Rick soit Morty
	 * @param prenom : Le prénom saisie par l'utilisaeur
	 * @param nom : Le nom saise par l'utilisateur
	 * @return : Le nouveau nom fictif de l'univers de Rick et Morty
	 */
	public static String TranslateRetM(String prenom, String nom){
		//On initialise la liste des noms
		List<String> nom_retm = new ArrayList<String>();
		nom_retm.add("Pickle ");	nom_retm.add("John "); 	nom_retm.add("Doofus ");
		nom_retm.add("Robot ");		nom_retm.add("Hawaiian "); 	nom_retm.add("Lizard ");
		nom_retm.add("Tiny ");		nom_retm.add("Teacher "); 	nom_retm.add("Simple ");
		nom_retm.add("Miami ");		nom_retm.add("Evil "); 		nom_retm.add("Flat Top ");
		nom_retm.add("Cyclops ");	nom_retm.add("Cop "); 		nom_retm.add("Bald ");
		nom_retm.add("Alien ");		nom_retm.add("Afro "); 		nom_retm.add("Karate ");
		nom_retm.add("Greaser ");	nom_retm.add("Vaporwave "); nom_retm.add("Beffcake ");
		nom_retm.add("Beard ");		nom_retm.add("Mustache "); 	nom_retm.add("Aqua ");
		nom_retm.add("Cowboy ");	nom_retm.add("Dandy ");

		String res = get_new_name(nom_retm, prenom);		

		//Puis on attribut le nouveau nom à res
		res = res + RickorMorty(nom);
		return res;
	}

	/**
	 * Retourne un prénom selon l'univers de Naruto
	 * Il n'y a besoin que du prenom et des 3 première lettre
	 * @param prenom : Le prenom saisie par l'utilisateur
	 * @return
	 */
	public static String TranslateNaruto(String prenom){
		String res = "";
		//On initialise chaque lettre par rapport à une syllabe
		List<String> list_naruto = new ArrayList<String>();
		list_naruto.add("ka");	list_naruto.add("zu"); 	list_naruto.add("mi");
		list_naruto.add("te");	list_naruto.add("ku"); 	list_naruto.add("lu");
		list_naruto.add("ji");	list_naruto.add("ri"); 	list_naruto.add("ki");
		list_naruto.add("zu");	list_naruto.add("me"); 	list_naruto.add("ta");
		list_naruto.add("rin");	list_naruto.add("to"); 	list_naruto.add("mo");
		list_naruto.add("no");	list_naruto.add("ke"); 	list_naruto.add("shi");
		list_naruto.add("ari");	list_naruto.add("chi"); list_naruto.add("do");
		list_naruto.add("ru");	list_naruto.add("mei"); list_naruto.add("na");
		list_naruto.add("fu");	list_naruto.add("zi");

		//On prend seulement les 3 première lettres du prénom
		for(int indice_prenom = 0; indice_prenom < 3; indice_prenom++){
			res = res + get_new_name(list_naruto, prenom.charAt(indice_prenom)+"");
		}

		//On met la première lettre en majscule
		String chaineMaj=res.replaceFirst(".",(res.charAt(0)+"").toUpperCase());

		return chaineMaj;
	}

	/**
	 * Retourne un nom et prénom selon l'univers de south park
	 * @param prenom : Le prenom saisie par l'utilisateur
	 * @param nom : Le nom saisie par l'utilisateur
	 * @return : Le nouveau nom et prénom fictif de l'utilisateur
	 */
	public static String TranslateSouthPark(String prenom, String nom){
		List<String> list_prenom = new ArrayList<String>();
		list_prenom.add("Chef");	list_prenom.add("Randy"); 	list_prenom.add("Butters");
		list_prenom.add("Stan");	list_prenom.add("Mimsy"); 	list_prenom.add("Nathan");
		list_prenom.add("Wendy");	list_prenom.add("Clyde"); 	list_prenom.add("Craig");
		list_prenom.add("Token");	list_prenom.add("Sharon"); 	list_prenom.add("Tweek");
		list_prenom.add("Timmy");	list_prenom.add("David");	list_prenom.add("Ike"); 	
		list_prenom.add("Eric");	list_prenom.add("Gerald");	list_prenom.add("Kyle"); 	
		list_prenom.add("Sheila");	list_prenom.add("Jimmy");	list_prenom.add("Stuart");  
		list_prenom.add("Kenny");	list_prenom.add("Barbrady");list_prenom.add("Harrison"); 
		list_prenom.add("Scoot");	list_prenom.add("Heidi");

		List<String> list_nom= new ArrayList<String>();
		list_nom.add("Marsh");		list_nom.add("Turner"); 	list_nom.add("Black");
		list_nom.add("Stotch");		list_nom.add("Tweak"); 		list_nom.add("Cartman");
		list_nom.add("?????");		list_nom.add("Tucker"); 	list_nom.add("Preston");
		list_nom.add("Testaburger");list_nom.add("Garrison"); 	list_nom.add("Biggle");
		list_nom.add("Daniels");	list_nom.add("McCormick");	list_nom.add("Rodriguez"); 	
		list_nom.add("Broflovski");	list_nom.add("Charlotte");	list_nom.add("Burch"); 	
		list_nom.add("Larsen");		list_nom.add("Knitts");		list_nom.add("Valmer");  
		list_nom.add("Stevens");	list_nom.add("Stoley");		list_nom.add("Donovan"); 
		list_nom.add("Mephesto");	list_nom.add("Malkinson");

		String res = get_new_name(list_prenom, prenom);
		res = res + " " + get_new_name(list_nom, nom);
		return res;
	}

	public static void main(String[] args) {
		InterfaceSaisie new_interface = new InterfaceSaisie();
	}
}
