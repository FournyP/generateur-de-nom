package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

public class InterfaceResultat extends JFrame implements ActionListener {
	
    private JPanel mainPanel;
    private GridBagConstraints gbc;
    private JLabel instruction;
    private JLabel resultat;
    private String nom_univers;
    private String res;
    private String nom_original;
    private JButton button_reessayer;
    private JButton button_quitter;

    public InterfaceResultat(String res, String nom_univers, String nom_original){
        gbc = new GridBagConstraints();
        this.nom_univers = (nom_univers == null) ? "null" : nom_univers;
        this.res = (res == null) ? "null" : res;
        this.nom_original = (nom_original == null) ? "null" : nom_original;
        initFrame();
    }

    public void initFrame(){
        this.setTitle("Générateur de nom");
        initPanel();
        this.setSize(600, 350);
		this.setResizable(false);
		this.setLocationRelativeTo(null); //On centre la fenetre à l'ecran
		this.setAlwaysOnTop(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Termine le programme si l'utilisateur clic sur la croix
        this.setVisible(true);
        this.setContentPane(mainPanel);
    }

    public void initPanel(){
        mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setVisible(true);
        mainPanel.setBackground(new Color(239,210,179));
        //On met gdc.fill horizontal pour que les composants s'étire
        gbc.fill = GridBagConstraints.HORIZONTAL;
        initLabel(); 
        initButton();
    }

    public void initLabel(){
        instruction = new JLabel("Votre nom " + this.nom_univers + " est :");
        instruction.setFont(new Font("Arial",Font.BOLD,18));
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(10,10,10,10);
        mainPanel.add(instruction, gbc);

        resultat = new JLabel(this.res);
        resultat.setFont(new Font("FreeSans",Font.PLAIN,26));
        gbc.gridx = 0;
        gbc.gridy = 1;
        mainPanel.add(resultat, gbc);
    }

    public void initButton(){
        button_reessayer = new JButton("Reessayer avec un autre univers");
		button_reessayer.setEnabled(true);
		//On positionne le bouton
		gbc.gridx = 0;
		gbc.gridy = 2;
		//insets permet de mettre du padding à nos composants
		gbc.insets = new Insets(50,0,0,10);
		mainPanel.add(button_reessayer, gbc);
        button_reessayer.addActionListener(this);
        
        button_quitter = new JButton("Quitter");
		button_reessayer.setEnabled(true);
		//On positionne le bouton
		gbc.gridx = 1;
		gbc.gridy = 2;
		//insets permet de mettre du padding à nos composants
		gbc.insets = new Insets(50,10,0,0);
		mainPanel.add(button_quitter, gbc);
        button_quitter.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button_reessayer){
            String[] nom_prenom = nom_original.split(" ");
            InterfaceChoixUnivers new_fenetre = new InterfaceChoixUnivers(nom_prenom[1], nom_prenom[0]);
            this.dispose();
        }
        if(e.getSource() == button_quitter){
            this.dispose();
        }
    }
}
