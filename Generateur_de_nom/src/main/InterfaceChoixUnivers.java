package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class InterfaceChoixUnivers extends JFrame implements ActionListener{
	
	private JPanel mainPanel;
	private GridBagConstraints gbc;
	private JButton univers1;
	private JButton univers2;
	private JButton univers3;
	private JLabel instruction;
	private String nom;
	private String prenom;
	
	public InterfaceChoixUnivers(String nom, String prenom) {
		gbc = new GridBagConstraints();
		initFrame();
		this.nom = (nom == null) ? "null" : nom;
		this.prenom = (prenom == null) ? "null" : prenom;
	}
	
	public void initFrame() {
		this.setTitle("Générateur de nom");
		initPanel();
		this.setSize(600, 350);
		this.setResizable(false);
		this.setLocationRelativeTo(null); //On centre la fenetre à l'ecran
		this.setAlwaysOnTop(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Termine le programme si l'utilisateur clic sur la croix
		this.setVisible(true);
		this.setContentPane(mainPanel);
	}

	public void initPanel(){
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setVisible(true);
		mainPanel.setBackground(new Color(239,210,179));
		//On met gdc.fill horizontal pour que les composants s'étire
		gbc.fill = GridBagConstraints.HORIZONTAL;
		initButton();
		initLabel();
	}

	public void initButton(){
		gbc.insets = new Insets(10,10,10,10);
		univers1 = new JButton("Naruto");
		univers1.setEnabled(true);
		gbc.gridx = 0;
		gbc.gridy = 1;
		mainPanel.add(univers1, gbc);

		univers2 = new JButton("Rick and Morty");
		univers2.setEnabled(true);
		gbc.gridx = 1;
		mainPanel.add(univers2, gbc);

		univers3 = new JButton("South Park");
		univers3.setEnabled(true);
		gbc.gridx = 2;
		mainPanel.add(univers3, gbc);

		univers1.addActionListener(this);
		univers2.addActionListener(this);
		univers3.addActionListener(this);
	}

	public void initLabel(){
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.insets = new Insets(0,0,50,0);
		instruction = new JLabel("Choisissez un univers :");
		instruction.setFont(new Font("Arial",Font.BOLD,18));
		mainPanel.add(instruction, gbc);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == univers1){
			InterfaceResultat new_interface = new InterfaceResultat(main.App.TranslateNaruto(prenom), "Naruto", prenom + " " + nom);
		}
		if(e.getSource() == univers2){
			InterfaceResultat new_interface = new InterfaceResultat(main.App.TranslateRetM(prenom, nom), "Rick and Morty", prenom + " " + nom);
		}
		if(e.getSource() == univers3){
			InterfaceResultat new_interface = new InterfaceResultat(main.App.TranslateSouthPark(prenom, nom), "South Park", prenom + " " + nom);
		}
		this.dispose();
	}
}
