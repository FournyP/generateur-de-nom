package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InterfaceSaisie extends JFrame implements KeyListener, ActionListener{

	private JPanel mainPanel;
	private GridBagConstraints gbc = new GridBagConstraints(); //Permet la gestion du panel avec des grilles
	private JLabel instruction;
	private JTextField saisie;
	private JButton button_next;
	
	InterfaceSaisie(){
		initFrame();
	}
	
	public void initFrame() {
		this.setTitle("Générateur de nom");
		initPanel();
		this.setSize(600, 350);
		this.setResizable(false);
		this.setLocationRelativeTo(null); //On centre la fenetre à l'ecran
		this.setAlwaysOnTop(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Termine le programme si l'utilisateur clic sur la croix
		this.setVisible(true);
		this.setContentPane(mainPanel);
	}
	
	public void initPanel() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setVisible(true);
		mainPanel.setBackground(new Color(239,210,179));
		//On met gdc.fill horizontal pour que les composants s'étire
		gbc.fill = GridBagConstraints.HORIZONTAL;
		initButton();
		initTextField();
		initLabel();
	}
	
	public void initButton() {
		button_next = new JButton("Suivant");
		button_next.setEnabled(false);
		//On positionne bouton
		gbc.gridx = 0;
		gbc.gridy = 2;
		//insets permet de mettre du padding à nos composants
		gbc.insets = new Insets(50,0,0,0);
		mainPanel.add(button_next, gbc);
		button_next.addActionListener(this);
	}
	
	public void initTextField() {
		saisie = new JTextField();
		//On positionne la zone de texte
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.insets = new Insets(0,0,20,0);
		mainPanel.add(saisie, gbc);
		saisie.addKeyListener(this);
	}
	
	public void initLabel() {
		instruction = new JLabel("Saisir votre pr�nom puis nom :");
		Font font = new Font("Arial",Font.BOLD,18);
		instruction.setFont(font);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0,0,70,0);
		mainPanel.add(instruction,gbc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == button_next){
			String[] nom_prenom = saisie.getText().split(" ");
			InterfaceChoixUnivers new_fenetre = new InterfaceChoixUnivers(nom_prenom[1], nom_prenom[0]);
			this.dispose();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		//Si la source provient du texte area permettant de saisir
		if(e.getSource() == saisie){
			//Et si le texte contenu dans le texte area est vérifié par la fonction ValiditeChaine
			//Alors on active le bouton pour passer à la suite
			if(main.App.ValiditeChaine(saisie.getText())){
				button_next.setEnabled(true);
			}
			else {
				button_next.setEnabled(false);
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
