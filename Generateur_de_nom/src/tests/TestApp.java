package tests;

import main.App;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestApp {


	@Test
	public void testget_new_name(){
		//Alphabet à l'envers
		List<String> list = new ArrayList<String>();
		list.add("Z");		list.add("Y"); 		list.add("X");
		list.add("W");		list.add("V"); 		list.add("U");
		list.add("T");		list.add("S"); 		list.add("R");
		list.add("Q");		list.add("P"); 		list.add("O");
		list.add("N");		list.add("M"); 		list.add("L");
		list.add("K");		list.add("J"); 		list.add("I");
		list.add("H");		list.add("G"); 		list.add("F");
		list.add("E");		list.add("D"); 		list.add("C");
		list.add("B");		list.add("A");
		//Retourne Z car retourne la première case de la liste list car A est la première lettre de l'alphabet
		assertEquals("A", main.App.get_new_name(list, "A"), "Z");
		assertEquals("B", main.App.get_new_name(list, "B"), "Y");
		assertEquals("Z", main.App.get_new_name(list, "Z"), "A");
	}
	@Test
	public void testValiditeChaine() {
		assertFalse("Pierre", main.App.ValiditeChaine("Pierre"));
		assertFalse("P1erre Fourny", main.App.ValiditeChaine("P1erre Fourny"));
		assertTrue("Pierre Fourny", main.App.ValiditeChaine("Pierre Fourny"));
		assertFalse("ABC", main.App.ValiditeChaine("ABC"));
		assertFalse("   ", main.App.ValiditeChaine("   "));
		assertFalse("AB C", main.App.ValiditeChaine("AB C"));
		assertTrue("ABC D", main.App.ValiditeChaine("ABC D"));
		assertFalse("ABC ", main.App.ValiditeChaine("ABC"));
	}

	@Test
	public void testRickorMorty(){
		assertEquals("Fourny", main.App.RickorMorty("Fourny"), "Rick");
		assertEquals("LeBastard", main.App.RickorMorty("Fourny"), "Rick");
		assertEquals("Gourny", main.App.RickorMorty("Gourny"), "Morty");
	}

	@Test
	public void testTranslateRetM(){
		assertEquals("A E", main.App.TranslateRetM("A", "E"), "Pickle Morty");
		assertEquals("Z E", main.App.TranslateRetM("Z", "E"), "Dandy Morty");
		assertEquals("Pierre Fourny", main.App.TranslateRetM("Pierre", "Fourny"), "Alien Rick");
		assertEquals("pierre fourny", main.App.TranslateRetM("pierre", "fourny"), "Alien Rick");
		assertEquals("Pierre Gourny", main.App.TranslateRetM("Pierre", "Gourny"), "Alien Morty");
		assertEquals("Valentin LeBastard", main.App.TranslateRetM("Valentin", "LeBastard"), "Beard Rick");
		assertEquals("Valentin MeBastard", main.App.TranslateRetM("Valentin", "MeBastard"), "Beard Morty");
		assertEquals("Vincent Morisse", main.App.TranslateRetM("Vincent", "Morisse"), "Beard Morty");
		assertEquals("Vincent Borisse", main.App.TranslateRetM("Vincent", "Borisse"), "Beard Rick");
	}

	@Test
	public void testTranslateNaruto(){
		assertEquals("Pierre Fourny", main.App.TranslateNaruto("Pierre"), "Nokiku");
		assertEquals("Valentin LeBastard", main.App.TranslateNaruto("Valentin"), "Rukata");
		assertEquals("Vincent Morisse", main.App.TranslateNaruto("Vincent"), "Rukito");
		assertEquals("Benji LaMenace", main.App.TranslateNaruto("Benji"), "Zukuto");
		assertEquals("ABC B", main.App.TranslateNaruto("ABC"), "Kazumi");
		assertEquals("XVN Z", main.App.TranslateNaruto("XVN"), "Naruto");
	}

	@Test
	public void testTranslateSouthPark(){
		assertEquals("Pierre Fourny", main.App.TranslateSouthPark("Pierre", "Fourny"), "Eric Cartman");
		assertEquals("Valentin Lebastard", main.App.TranslateSouthPark("Valentin", "Lebastard"), "Kenny Biggle");
		assertEquals("Vincent Morisse", main.App.TranslateSouthPark("Vincent", "Morisse"), "Kenny Daniels");
		assertEquals("Benji LaMenace", main.App.TranslateSouthPark("Benji", "LaMenace"), "Randy Biggle");
	}
}
